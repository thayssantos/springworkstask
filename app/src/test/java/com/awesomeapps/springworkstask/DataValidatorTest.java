package com.awesomeapps.springworkstask;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotEquals;

import com.awesomeapps.springworkstask.datavalidator.DataValidator;
import com.awesomeapps.springworkstask.model.AverageConsumption;
import com.awesomeapps.springworkstask.model.Co2;
import com.awesomeapps.springworkstask.model.Emission;
import com.awesomeapps.springworkstask.model.Fuel;
import com.awesomeapps.springworkstask.model.Gasoline;
import com.awesomeapps.springworkstask.model.GasolineFuel;
import com.awesomeapps.springworkstask.model.VehicleAttributes;

import org.junit.Before;
import org.junit.Test;

import java.math.BigDecimal;
import java.util.Arrays;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */
public class DataValidatorTest {

    private DataValidator dataValidator;

    @Before
    public void setUp() throws Exception {

        dataValidator = new DataValidator();
    }

    @Test
    public void validateAttributesObjectComplete() throws Exception {
        VehicleAttributes vehicleAttributes = new VehicleAttributes(
                "wur816",
                "tmbga61z852094863",
                "2015-05-29T12:18:31.390Z",
                new Emission(new Gasoline(new Co2(new BigDecimal(0.000175)))),
                new Fuel(new GasolineFuel(new AverageConsumption(
                        new BigDecimal(0.000099),
                        new BigDecimal(0.000058),
                        new BigDecimal(0.000073)))),
                "manual",
                2005,
                "volvo",
                Arrays.asList("gasoline"));

        assertEquals(dataValidator.validateAttributes(vehicleAttributes), vehicleAttributes);
    }

    @Test
    public void validateAttributesNullAttributes() throws Exception {
        VehicleAttributes vehicleAttributes = new VehicleAttributes(
                null,
                null,
                null,
                null,
                null,
                null,
                0,
                null,
                null);

        assertEquals(dataValidator.validateAttributes(vehicleAttributes), vehicleAttributes);

    }

    @Test
    public void validateAttributesObjectNull() throws Exception {
        VehicleAttributes vehicleAttributes = null;

        assertEquals(dataValidator.validateAttributes(vehicleAttributes), vehicleAttributes);
    }


    @Test
    public void validateAttributesInvalidValues() throws Exception {
        VehicleAttributes vehicleAttributes = new VehicleAttributes(
                "wur816",
                "tmbga61z852094863",
                "2015-05-29T12:18:31.390Z",
                new Emission(new Gasoline(new Co2(new BigDecimal(0.000175)))),
                new Fuel(new GasolineFuel(new AverageConsumption(
                        new BigDecimal(0.000099),
                        new BigDecimal(0.000058),
                        new BigDecimal(0.000073)))),
                "wrong",
                2005,
                "wrong",
                Arrays.asList("gasoline", "alcohol"));

        VehicleAttributes vehicleAttributesTest = new VehicleAttributes(
                "wur816",
                "tmbga61z852094863",
                "2015-05-29T12:18:31.390Z",
                new Emission(new Gasoline(new Co2(new BigDecimal(0.000175)))),
                new Fuel(new GasolineFuel(new AverageConsumption(
                        new BigDecimal(0.000099),
                        new BigDecimal(0.000058),
                        new BigDecimal(0.000073)))),
                "wrong",
                2005,
                "wrong",
                Arrays.asList("gasoline", "alcohol"));

        assertNotEquals(dataValidator.validateAttributes(vehicleAttributes), vehicleAttributesTest);
    }

}