package com.awesomeapps.springworkstask;

import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import com.awesomeapps.springworkstask.data.DataManager;
import com.awesomeapps.springworkstask.model.AverageConsumption;
import com.awesomeapps.springworkstask.model.Co2;
import com.awesomeapps.springworkstask.model.Emission;
import com.awesomeapps.springworkstask.model.Fuel;
import com.awesomeapps.springworkstask.model.Gasoline;
import com.awesomeapps.springworkstask.model.GasolineFuel;
import com.awesomeapps.springworkstask.model.VehicleAttributes;
import com.awesomeapps.springworkstask.ui.VehicleAttributeFragment;
import com.awesomeapps.springworkstask.ui.VehicleAttributePresenter;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.math.BigDecimal;
import java.util.Arrays;

import io.reactivex.Observable;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

@RunWith(MockitoJUnitRunner.Silent.class)
public class VehicleAttributePresenterTest {
    VehicleAttributePresenter presenter;

    @Mock
    private DataManager dataManager;

    @Mock
    private VehicleAttributeFragment view;

    @Before
    public void setup() throws Exception {
        MockitoAnnotations.initMocks(this);
        presenter = new VehicleAttributePresenter(view, dataManager);
    }

    @Test
    public void fetchValidDataShouldLoadIntoView() throws Exception {

        VehicleAttributes vehicleAttributes = new VehicleAttributes(
                "wur816",
                "tmbga61z852094863",
                "2015-05-29T12:18:31.390Z",
                new Emission(new Gasoline(new Co2(new BigDecimal(0.000175)))),
                new Fuel(new GasolineFuel(new AverageConsumption(
                        new BigDecimal(0.000099),
                        new BigDecimal(0.000058),
                        new BigDecimal(0.000073)))),
                "manual",
                2005,
                "volvo",
                Arrays.asList("gasoline"));


        when(dataManager.createObservable(""))
                .thenReturn(Observable.just(vehicleAttributes));

        presenter.loadVehicleAttributes("");

        verify(view, never()).showFailureToast();
    }

    @Test
    public void fetchErrorShouldReturnErrorToView() throws Exception {
        Exception exception = new Exception();

        when(dataManager.createObservable(""))
                .thenReturn(Observable.<VehicleAttributes>error(exception));

        presenter.loadVehicleAttributes("");
        verify(view, never()).showAttributes(null);
    }
}
