package com.awesomeapps.springworkstask.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import com.awesomeapps.springworkstask.R;
import com.awesomeapps.springworkstask.data.DataManager;
import com.awesomeapps.springworkstask.datavalidator.FuelType;
import com.awesomeapps.springworkstask.model.AverageConsumption;
import com.awesomeapps.springworkstask.model.Co2;
import com.awesomeapps.springworkstask.model.Emission;
import com.awesomeapps.springworkstask.model.VehicleAttributes;

/**
 * Created by Thays Santos Duarte on 06/02/2018.
 */

public class VehicleAttributeFragment extends Fragment {

    private VehicleAttributePresenter presenter;
    private View view;
    private TextView regnoTxt;
    private TextView vinTxt;
    private TextView brandTxt;
    private TextView yearTxt;
    private TextView gearboxTypeTxt;
    private TextView fuelTypesTxt;
    private TextView gasolineInfo;
    private TextView gasolineInfoTxt;
    private TextView dieselInfo;
    private TextView dieselInfoTxt;
    private TextView emissionTxt;

    private ProgressBar progressBar;
    private VehicleAttributes vehicleAttributes;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        presenter = new VehicleAttributePresenter(this, DataManager.getInstance());
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container,
            @Nullable Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.frag_vehicle_attributes, container, false);

        regnoTxt = (TextView) view.findViewById(R.id.regno_attr_txt);
        vinTxt = (TextView) view.findViewById(R.id.vin_attr_txt);
        brandTxt = (TextView) view.findViewById(R.id.brand_attr_txt);
        yearTxt = (TextView) view.findViewById(R.id.year_attr_txt);
        gearboxTypeTxt = (TextView) view.findViewById(R.id.gearbox_type_attr_txt);
        fuelTypesTxt = (TextView) view.findViewById(R.id.fuel_type_attr_txt);
        gasolineInfo = (TextView) view.findViewById(R.id.gasoline_info);
        gasolineInfoTxt = (TextView) view.findViewById(R.id.gasoline_info_attr_txt);
        dieselInfo = (TextView) view.findViewById(R.id.diesel_info);
        dieselInfoTxt = (TextView) view.findViewById(R.id.diesel_info_attr_txt);
        emissionTxt = (TextView) view.findViewById(R.id.emission_attr_txt);
        progressBar = (ProgressBar) view.findViewById(R.id.progressbar);

        loadAttributes();

        return view;
    }

    public void showAttributes(VehicleAttributes vehicleAttributes) {
        this.vehicleAttributes = vehicleAttributes;

        regnoTxt.setText(vehicleAttributes.getRegno());
        vinTxt.setText(vehicleAttributes.getVin());
        brandTxt.setText(vehicleAttributes.getBrand());
        yearTxt.setText(String.valueOf(vehicleAttributes.getYear()));
        gearboxTypeTxt.setText(vehicleAttributes.getGearboxType());
        setFuelTypesAndGasDieselInfo(vehicleAttributes);
        setEmissionInfo(vehicleAttributes.getEmission());

    }

    public void setEmissionInfo(Emission emissionInfo) {

        if (emissionInfo.getDiesel() != null) {
            createCO2emissionTxt(emissionInfo.getDiesel().getCo2());
        } else if (emissionInfo.getGasoline() != null) {
            createCO2emissionTxt(emissionInfo.getGasoline().getCo2());
        }

    }

    private void createCO2emissionTxt(Co2 co2) {
        StringBuilder emission = new StringBuilder();

        if (co2.getUrban() != null) {
            emission.append(getResources().getString(R.string.emission_info_urban));
            emission.append(co2.getUrban());
            emission.append("\n");
        }
        if (co2.getRural() != null) {
            emission.append(getResources().getString(R.string.emission_info_rural));
            emission.append(co2.getRural());
            emission.append("\n");
        }
        if (co2.getMixed() != null) {
            emission.append(getResources().getString(R.string.emission_info_mixed));
            emission.append(co2.getMixed());
            emission.append("\n");
        }

        emissionTxt.setText(emission);
    }

    private void setFuelTypesAndGasDieselInfo(VehicleAttributes vehicleAttributes) {
        StringBuilder fuelTypes = new StringBuilder();
        for (String fuel : vehicleAttributes.getFuelTypes()) {
            fuelTypes.append(fuel);
            fuelTypes.append("; ");

            if (FuelType.GASOLINE.name().equalsIgnoreCase(fuel)) {
                gasolineInfo.setVisibility(View.VISIBLE);
                gasolineInfoTxt.setVisibility(View.VISIBLE);
                dieselInfo.setVisibility(View.GONE);
                dieselInfoTxt.setVisibility(View.GONE);

                gasolineInfoTxt.setText(createGasDieselInfoText(
                        vehicleAttributes.getFuel().getGasoline().getAverageConsumption(),
                        vehicleAttributes.getFuel().getGasoline().getTankVolume()));
            }
            if (FuelType.DIESEL.name().equalsIgnoreCase(fuel)) {
                gasolineInfo.setVisibility(View.GONE);
                gasolineInfoTxt.setVisibility(View.GONE);
                dieselInfo.setVisibility(View.VISIBLE);
                dieselInfoTxt.setVisibility(View.VISIBLE);

                dieselInfoTxt.setText(createGasDieselInfoText(
                        vehicleAttributes.getFuel().getDiesel().getAverageConsumption(),
                        vehicleAttributes.getFuel().getDiesel().getTankVolume()));
            }
        }
        fuelTypesTxt.setText(fuelTypes);
    }

    private StringBuilder createGasDieselInfoText(AverageConsumption averageConsumption,
            Double tankVolume) {
        StringBuilder gasInfo = new StringBuilder();
        if (averageConsumption.getUrban() != null) {
            gasInfo.append(getResources().getString(R.string.fuel_info_urban));
            gasInfo.append(averageConsumption.getUrban());
            gasInfo.append("\n");
        }
        if (averageConsumption.getRural() != null) {
            gasInfo.append(getResources().getString(R.string.fuel_info_rural));
            gasInfo.append(averageConsumption.getRural());
            gasInfo.append("\n");
        }
        if (averageConsumption.getMixed() != null) {
            gasInfo.append(getResources().getString(R.string.fuel_info_mixed));
            gasInfo.append(averageConsumption.getMixed());
            gasInfo.append("\n");
        }
        if (tankVolume != null) {
            gasInfo.append(getResources().getString(R.string.tank_volume));
            gasInfo.append(tankVolume);
        }

        return gasInfo;
    }

    public void showFailureToast() {
        progressBar.setVisibility(View.GONE);
        Toast.makeText(getContext(), "Something wrong happened. Try again",
                Toast.LENGTH_LONG).show();
    }

    public void fetchOnCompleted() {
        progressBar.setVisibility(View.GONE);
    }

    public void loadAttributes() {
        presenter.loadVehicleAttributes("");
    }

    public VehicleAttributes getVehicleAttributes() {
        return vehicleAttributes;
    }

}
