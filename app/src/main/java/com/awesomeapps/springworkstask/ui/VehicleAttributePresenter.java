package com.awesomeapps.springworkstask.ui;

import android.util.Log;

import com.awesomeapps.springworkstask.data.DataManager;
import com.awesomeapps.springworkstask.data.GetVehicleAttrCallback;
import com.awesomeapps.springworkstask.datavalidator.DataValidator;
import com.awesomeapps.springworkstask.model.VehicleAttributes;

/**
 * Created by Thays Santos Duarte on 06/02/2018.
 */

public class VehicleAttributePresenter {

    private final String TAG = getClass().getSimpleName();
    private final VehicleAttributeFragment view;
    private DataManager dataManager;
    private VehicleAttributes vehicleAttributes;

    public VehicleAttributePresenter(VehicleAttributeFragment view, DataManager dataManager) {
        this.view = view;
        this.dataManager = dataManager;
    }

    public void loadVehicleAttributes(String vin) {
        if (view != null) {
            dataManager.getVehicleAttributes(vin, new GetVehicleAttrCallback() {
                @Override
                public void onSuccess(VehicleAttributes vehicleAttributes) {
                    view.showAttributes(DataValidator.validateAttributes(vehicleAttributes));
                }

                @Override
                public void onFailure(Throwable throwable) {
                    Log.e(TAG, throwable.getMessage());
                    view.showFailureToast();
                }

                @Override
                public void onComplete() {
                    view.fetchOnCompleted();
                }
            });
        }
    }
}
