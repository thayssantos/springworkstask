package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class DieselFuel {

    @SerializedName("average_consumption")
    @Expose
    private AverageConsumption averageConsumption;
    @SerializedName("tank_volume")
    @Expose
    private Double tankVolume;

    public AverageConsumption getAverageConsumption() {
        return averageConsumption;
    }

    public void setAverageConsumption(AverageConsumption averageConsumption) {
        this.averageConsumption = averageConsumption;
    }

    public Double getTankVolume() {
        return tankVolume;
    }

    public void setTankVolume(Double tankVolume) {
        this.tankVolume = tankVolume;
    }
}
