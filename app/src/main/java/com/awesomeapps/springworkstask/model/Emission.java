package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Emission {

    @SerializedName("gasoline")
    @Expose
    private Gasoline gasoline;
    @SerializedName("diesel")
    @Expose
    private Diesel diesel;

    public Emission(Gasoline gasoline) {
        this.gasoline = gasoline;
    }

    public Gasoline getGasoline() {
        return gasoline;
    }

    public void setGasoline(Gasoline gasoline) {
        this.gasoline = gasoline;
    }

    public Diesel getDiesel() {
        return diesel;
    }

    public void setDiesel(Diesel diesel) {
        this.diesel = diesel;
    }
}
