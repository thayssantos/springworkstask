package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class Co2 {

    @SerializedName("mixed")
    @Expose
    private BigDecimal mixed;
    @SerializedName("rural")
    @Expose
    private BigDecimal rural;
    @SerializedName("urban")
    @Expose
    private BigDecimal urban;

    public Co2(BigDecimal mixed) {
        this.mixed = mixed;
    }

    public BigDecimal getMixed() {
        return mixed;
    }

    public void setMixed(BigDecimal mixed) {
        this.mixed = mixed;
    }

    public BigDecimal getRural() {
        return rural;
    }

    public void setRural(BigDecimal rural) {
        this.rural = rural;
    }

    public BigDecimal getUrban() {
        return urban;
    }

    public void setUrban(BigDecimal urban) {
        this.urban = urban;
    }
}
