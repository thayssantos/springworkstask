package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;

public class AverageConsumption {

    @SerializedName("urban")
    @Expose
    private BigDecimal urban;
    @SerializedName("rural")
    @Expose
    private BigDecimal rural;
    @SerializedName("mixed")
    @Expose
    private BigDecimal mixed;

    public AverageConsumption(BigDecimal urban, BigDecimal rural, BigDecimal mixed) {
        this.urban = urban;
        this.rural = rural;
        this.mixed = mixed;
    }

    public BigDecimal getUrban() {
        return urban;
    }

    public void setUrban(BigDecimal urban) {
        this.urban = urban;
    }

    public BigDecimal getRural() {
        return rural;
    }

    public void setRural(BigDecimal rural) {
        this.rural = rural;
    }

    public BigDecimal getMixed() {
        return mixed;
    }

    public void setMixed(BigDecimal mixed) {
        this.mixed = mixed;
    }

}
