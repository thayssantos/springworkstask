package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Diesel {

    @SerializedName("co2")
    @Expose
    private Co2 co2;

    public Co2 getCo2() {
        return co2;
    }

    public void setCo2(Co2 co2) {
        this.co2 = co2;
    }
}
