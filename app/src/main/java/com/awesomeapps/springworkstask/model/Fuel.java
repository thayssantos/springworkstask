package com.awesomeapps.springworkstask.model;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Fuel {

    @SerializedName("gasoline")
    @Expose
    private GasolineFuel gasoline;
    @SerializedName("diesel")
    @Expose
    private DieselFuel diesel;

    public Fuel(GasolineFuel gasolineFuel) {
        this.gasoline = gasolineFuel;
    }

    public GasolineFuel getGasoline() {
        return gasoline;
    }

    public void setGasoline(GasolineFuel gasoline) {
        this.gasoline = gasoline;
    }

    public DieselFuel getDiesel() {
        return diesel;
    }

    public void setDiesel(DieselFuel diesel) {
        this.diesel = diesel;
    }
}
