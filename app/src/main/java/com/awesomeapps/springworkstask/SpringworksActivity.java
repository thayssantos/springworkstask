package com.awesomeapps.springworkstask;

import android.os.Bundle;

import com.awesomeapps.springworkstask.ui.VehicleAttributeFragment;
import com.awesomeapps.springworkstask.util.BaseActivity;

public class SpringworksActivity extends BaseActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_springworks);
        showFragment(VehicleAttributeFragment.class);
    }
}
