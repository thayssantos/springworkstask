package com.awesomeapps.springworkstask.datavalidator;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

public enum GearBoxType {

    MANUAL,
    AUTOMATIC;

    static boolean isGearBoxType(String gearBoxType) {
        for (GearBoxType gear : GearBoxType.values()) {
            if (gear.name().equalsIgnoreCase(gearBoxType)) {
                return true;
            }
        }
        return false;
    }
}
