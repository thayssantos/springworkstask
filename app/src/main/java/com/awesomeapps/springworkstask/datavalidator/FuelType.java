package com.awesomeapps.springworkstask.datavalidator;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

public enum FuelType {

    GASOLINE,
    DIESEL;

    static boolean isFuelType(String fuel) {
        for (FuelType fuelType : FuelType.values()) {
            if (fuelType.name().equalsIgnoreCase(fuel)) {
                return true;
            }
        }
        return false;
    }
}
