package com.awesomeapps.springworkstask.datavalidator;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

public enum Brand {

    VOLVO,
    FORD,
    SAAD,
    ROVER;

    static boolean isBrand(String brand) {
        for (Brand b : Brand.values()) {
            if (b.name().equalsIgnoreCase(brand)) {
                return true;
            }
        }
        return false;
    }
}
