package com.awesomeapps.springworkstask.datavalidator;

import com.awesomeapps.springworkstask.model.VehicleAttributes;

import java.util.List;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

public class DataValidator {

    public static VehicleAttributes validateAttributes(VehicleAttributes vehicleAttributes) {
        VehicleAttributes vehicleAttributesCopy = vehicleAttributes;

        if (vehicleAttributes != null) {
            if (vehicleAttributes.getRegno() != null) {
                vehicleAttributesCopy.setRegno(validateRegnoSize(vehicleAttributes.getRegno()));
            }

            if (vehicleAttributes.getBrand() != null) {
                vehicleAttributesCopy.setBrand(validateBrand(vehicleAttributes.getBrand()));
            }

            if (vehicleAttributes.getGearboxType() != null) {
                vehicleAttributesCopy.setGearboxType(
                        validateGearBoxType(vehicleAttributes.getGearboxType()));
            }

            if (vehicleAttributes.getFuelTypes() != null) {
                vehicleAttributesCopy.setFuelTypes(
                        validateFuelTypes(vehicleAttributes.getFuelTypes()));
            }
        }

        return vehicleAttributesCopy;
    }

    private static List<String> validateFuelTypes(List<String> fuelTypes) {
        for (int i = 0; i < fuelTypes.size(); i++) {
            if (!FuelType.isFuelType(fuelTypes.get(i))) {
                fuelTypes.set(i, "invalid value");
            }
        }
        return fuelTypes;
    }

    private static String validateGearBoxType(String gearboxType) {
        String res = "-";

        if (GearBoxType.isGearBoxType(gearboxType)) {
            res = gearboxType;
        }
        return res;
    }

    private static String validateBrand(String brand) {
        String res = "-";

        if (Brand.isBrand(brand)) {
            res = brand;
        }
        return res;
    }

    private static String validateRegnoSize(String regno) {
        String res = "Invalid Registration Number";

        if (regno.length() >= 6 && regno.length() <= 7) {
            res = regno;
        }
        return res;
    }
}
