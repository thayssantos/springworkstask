package com.awesomeapps.springworkstask.data;

import com.awesomeapps.springworkstask.model.VehicleAttributes;

/**
 * Created by Thays Santos Duarte on 10/02/2018.
 */

public interface GetVehicleAttrCallback {
    void onSuccess(VehicleAttributes vehicleAttributes);

    void onFailure(Throwable throwable);

    void onComplete();
}
