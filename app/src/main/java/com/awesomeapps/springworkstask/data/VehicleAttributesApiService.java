package com.awesomeapps.springworkstask.data;

import com.awesomeapps.springworkstask.model.VehicleAttributes;
import com.awesomeapps.springworkstask.util.Constants;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Path;

/**
 * Created by Thays Santos Duarte on 09/02/2018.
 */

public interface VehicleAttributesApiService {

    @GET(Constants.GET_ATTR_VEHICLES_END_POINT)
    Observable<VehicleAttributes> getVehicleAttributes(@Path(Constants.VIN) String vin);

    @GET(Constants.MOCK_PATH)
    Observable<VehicleAttributes> getVehicleAttributesStatic();
}
