package com.awesomeapps.springworkstask.data;

import com.awesomeapps.springworkstask.model.VehicleAttributes;
import com.awesomeapps.springworkstask.util.Constants;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Thays Santos Duarte on 08/02/2018.
 */

public class DataManager {

    private final String TAG = getClass().getSimpleName();
    private static DataManager instance = new DataManager();
    private final VehicleAttributesApiService vehicleAttributesApiService;
    private Retrofit retrofitClient;
    private CompositeDisposable compositeDisposable;

    public static DataManager getInstance() {
        return instance;
    }

    public DataManager() {
        retrofitClient = createRetrofit();
        vehicleAttributesApiService = retrofitClient.create(VehicleAttributesApiService.class);
        compositeDisposable = new CompositeDisposable();
    }

    public void getVehicleAttributes(String vin, GetVehicleAttrCallback getVehicleAttrCallback) {
        compositeDisposable.add(createObservable(vin)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(createObserver(getVehicleAttrCallback)));
    }

    private DisposableObserver<VehicleAttributes> createObserver(
            final GetVehicleAttrCallback getVehicleAttrCallback) {
        return new DisposableObserver<VehicleAttributes>() {
            @Override
            public void onNext(VehicleAttributes response) {
                getVehicleAttrCallback.onSuccess(response);
            }

            @Override
            public void onError(Throwable e) {
                getVehicleAttrCallback.onFailure(e);
            }

            @Override
            public void onComplete() {
                getVehicleAttrCallback.onComplete();
            }
        };
    }

    public Observable<VehicleAttributes> createObservable(String vin) {
        /*In a real case this would be used*/
        //return vehicleAttributesApiService.loadVehicleAttributes(vin);

        /*Using a static path*/
        return vehicleAttributesApiService.getVehicleAttributesStatic();
    }

    private Retrofit createRetrofit() {
        return new Retrofit.Builder()
                .baseUrl(Constants.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                .client(createOkHttpClient())
                .build();
    }

    private OkHttpClient createOkHttpClient() {
        HttpLoggingInterceptor logging = new HttpLoggingInterceptor();
        logging.setLevel(HttpLoggingInterceptor.Level.BODY);

        final OkHttpClient.Builder httpClient = new OkHttpClient.Builder();
        httpClient.addInterceptor(logging);
        return httpClient.build();
    }
}
