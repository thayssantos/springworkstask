package com.awesomeapps.springworkstask.util;

/**
 * Created by Thays Santos Duarte on 09/02/2018.
 */

public class Constants {

    public static final String GET_ATTR_VEHICLES_END_POINT =
            "/v1/vehicles/{vin}/vehicle_attributes";
    public static final String VIN = "vin";

    public static final String MOCK_PATH = "sommestad/e38c1acf2aed495edf2d/raw/"
            + "cdb6dfb85101eedad60853c44266249a3f4ac5df/vehicle-attributes.json/";
    public static final String BASE_URL = "https://gist.githubusercontent.com/";
}
